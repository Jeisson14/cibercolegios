webpackJsonp([0],{

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShooseProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_helper__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ShooseProfilePage = (function () {
    function ShooseProfilePage(navCtrl, params, helper) {
        this.navCtrl = navCtrl;
        this.params = params;
        this.helper = helper;
        this.profiles = {};
        console.log('que trae', this.params.get('id'));
        this.id = this.params.data.login.id;
        this.name = this.params.data.login.name;
        this.email = this.params.data.login.email;
        this.profiles = this.params.data.login.profiles;
        this.profileId = this.params.data.login.profiles.profileId;
        this.institutionId = this.params.data.login.profiles.institutionId;
        this.institutionDesc = this.params.data.login.profiles.institutionDesc;
        this.profileDesc = this.params.data.login.profiles.profileDesc;
        var id = params.get('id');
        // if (undefined === id) {
        //   this.helper.app.initializePages();
        // } else {
        //   this.id = id;
        // }
    }
    ShooseProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-shoose-profile',template:/*ion-inline-start:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/shoose-profile/shoose-profile.html"*/'<ion-header>\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>Shoose Profile</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n    <ion-list>\n      <ion-item-sliding>\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="../../assets/imgs/logo.png ">\n          </ion-avatar>\n          <h2>Colegio</h2>\n          <p>perfil 1</p>\n          <p>perfil 2</p>\n        </ion-item>\n        <ion-item-options side="left">\n          <button ion-button color="primary">\n            <ion-icon name="school-outline"></ion-icon>\n            Cargo\n          </button>\n          <button ion-button color="secondary">\n            <ion-icon name="school-outline"></ion-icon>\n            Cargo 1\n          </button>\n        </ion-item-options>\n        <ion-item-options side="right">\n          <button ion-button color="primary">\n            <ion-icon name="school-outline"></ion-icon>\n            Cargo 3\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n      <ion-item *ngIf="login.id">\n        <ion-label stacked for="id" class="serviw-label">\n          <span>\n            id\n        </span>\n        </ion-label>\n        <ion-label stacked for="id" class="padd"> {{ login.id }} </ion-label>\n      </ion-item>\n      <ion-item *ngIf="login.name">\n        <ion-label stacked for="name" class="serviw-label">\n          <span>\n            name\n        </span>\n        </ion-label>\n        <ion-label stacked for="name" class="padd"> {{ login.name }} </ion-label>\n      </ion-item>\n      <ion-item *ngIf="login.email">\n        <ion-label stacked for="email" class="serviw-label">\n          <span>\n            email\n        </span>\n        </ion-label>\n        <ion-label stacked for="email" class="padd"> {{ login.email }} </ion-label>\n      </ion-item>\n    </ion-list>\n  </ion-content>'/*ion-inline-end:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/shoose-profile/shoose-profile.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__utils_helper__["a" /* Helper */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__utils_helper__["a" /* Helper */]) === "function" && _c || Object])
    ], ShooseProfilePage);
    return ShooseProfilePage;
    var _a, _b, _c;
}());

//# sourceMappingURL=shoose-profile.js.map

/***/ }),

/***/ 130:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 130;

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item-sliding>\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="../../assets/imgs/logo.png ">\n        </ion-avatar>\n        <h2>Colegio</h2>\n        <p>perfil 1</p>\n        <p>perfil 2</p>\n      </ion-item>\n      <ion-item-options side="left">\n        <button ion-button color="primary">\n          <ion-icon name="school-outline"></ion-icon>\n          Cargo\n        </button>\n        <button ion-button color="secondary">\n          <ion-icon name="school-outline"></ion-icon>\n          Cargo 1\n        </button>\n      </ion-item-options>\n      <ion-item-options side="right">\n        <button ion-button color="primary">\n          <ion-icon name="school-outline"></ion-icon>\n          Cargo 3\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ProfilePage_1 = ProfilePage;
    ProfilePage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ProfilePage_1, {
            item: item
        });
    };
    ProfilePage = ProfilePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/profile/profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
    var ProfilePage_1;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_helper__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_apollo_angular__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_graphql_tag__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_graphql_tag___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_graphql_tag__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shoose_profile_shoose_profile__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__profile_profile__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_apollo_angular_link_http__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_apollo_cache_inmemory__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_apollo_link__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var Login = (_a = ["\n  query Login ($username:String!, $password:String!, $mobileId:String!){\n    login(username: $username, password: $password, mobileId: $mobileId) {\n      token\n      id\n      name\n      lastName\n      passwordChange\n      email\n      profiles{\n        profileIdCon\n        profileId\n        institutionId\n        institutionDesc\n        profileDesc\n      }\n    }\n  }"], _a.raw = ["\n  query Login ($username:String!, $password:String!, $mobileId:String!){\n    login(username: $username, password: $password, mobileId: $mobileId) {\n      token\n      id\n      name\n      lastName\n      passwordChange\n      email\n      profiles{\n        profileIdCon\n        profileId\n        institutionId\n        institutionDesc\n        profileDesc\n      }\n    }\n  }"], __WEBPACK_IMPORTED_MODULE_6_graphql_tag___default()(_a));
var LoginPage = (function () {
    function LoginPage(formBuilder, navCtrl, navParams, http, helper, apollo, httpLink, platform) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.helper = helper;
        this.apollo = apollo;
        this.httpLink = httpLink;
        this.platform = platform;
        this.model = {};
        this.mobile = "MOB-88768";
        if (this.platform.is('core')) {
            this.protocol = "http";
            this.host = "localhost:8100/cibercolegios";
        }
        else {
            this.protocol = "https";
            this.host = "appstest.cibercolegios.com";
        }
        this.home = this.protocol + "://" + this.host;
        this.ionViewDidLoad();
        var uri = this.home + '/api/security/';
        var method = 'POST';
        // const headers = new HttpHeaders ({
        //   'Content-Type': 'application/json'
        // });
        var authMiddleware = new __WEBPACK_IMPORTED_MODULE_11_apollo_link__["a" /* ApolloLink */](function (operation, forward) {
            operation.setContext({
                headers: {
                    'Content-Type': 'text/plain'
                }
            });
            return forward(operation);
        });
        var queryLink = httpLink.create({ uri: uri, method: method });
        apollo.create({
            link: Object(__WEBPACK_IMPORTED_MODULE_11_apollo_link__["c" /* concat */])(authMiddleware, queryLink),
            cache: new __WEBPACK_IMPORTED_MODULE_10_apollo_cache_inmemory__["a" /* InMemoryCache */](),
        });
        // this.user = navParams.data || new User();
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        this.loginForm = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
        });
    };
    LoginPage.prototype.sendLogin = function () {
        var _this = this;
        this.helper.toastMessage('Iniciando Session');
        var body = {
            username: this.loginForm.value.username,
            password: this.loginForm.value.password,
            mobileId: this.mobile
        };
        var params = Object.assign(body);
        console.log('los campos son ', params);
        var loginQuery = this.apollo.query({ query: Login, variables: params })
            .subscribe(function (data) { return _this.loginOk(data); }, function (errors) { return _this.loginError(errors); });
    };
    LoginPage.prototype.loginOk = function (queryResult) {
        var _this = this;
        var query = queryResult.data.login;
        console.log('query final', query);
        if (query.token &&
            query.id &&
            query.name &&
            query.email &&
            query.profiles) {
            this.helper.saveUser(query.token, query.id, query.name, query.email, query.profiles).then(function (val) {
                if (_this.helper.NONE === _this.helper.id) {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__profile_profile__["a" /* ProfilePage */]);
                }
                else {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__shoose_profile_shoose_profile__["a" /* ShooseProfilePage */]);
                }
            }, function (error) {
                console.log('Error on login Ok save the user');
                console.log(error);
            });
        }
    };
    LoginPage.prototype.loginError = function (queryError) {
        var errors = queryError.errors;
        console.log('error del login', errors);
        this.navCtrl.setRoot(LoginPage_1);
    };
    LoginPage.prototype.saveUser = function (user) {
    };
    LoginPage = LoginPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/login/login.html"*/'<ion-content class="wallpaper">\n    <form [formGroup]="loginForm" (ngSubmit)="sendLogin()">\n      <ion-list class="list-login">\n        <ion-item class="item-list-username-login">\n          <ion-label floating for="username">\n            Usuario\n          </ion-label>\n          <ion-input formControlName="username" type="text" name="username" placeholder="Usuario"></ion-input>\n        </ion-item>\n        <ion-item class="item-list-password-login">\n          <ion-label floating for="password">\n            Contraseña\n          </ion-label>\n          <ion-input formControlName="password" type="password" name="password" placeholder="Contraseña"></ion-input>\n        </ion-item>\n      </ion-list>\n      <div class="div-button">\n        <button ion-button class="button-login"  [disabled]="!loginForm.valid" type="submit">INGRESAR</button>\n      </div>\n    </form>\n    <div class="terminos">\n      <p>Al ingresar aceptas nuestros</p>\n      <a [href]="">Términos y condiciones</a>\n    <br>\n      <a [href]="resetLink">¿Olvidaste tu Contraseña</a>\n    </div>\n</ion-content>'/*ion-inline-end:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__utils_helper__["a" /* Helper */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__utils_helper__["a" /* Helper */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5_apollo_angular__["a" /* Apollo */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_apollo_angular__["a" /* Apollo */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_9_apollo_angular_link_http__["a" /* HttpLink */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9_apollo_angular_link_http__["a" /* HttpLink */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* Platform */]) === "function" && _h || Object])
    ], LoginPage);
    return LoginPage;
    var LoginPage_1, _a, _b, _c, _d, _e, _f, _g, _h;
}());

var _a;
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(263);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_helper__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_apollo_angular__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_apollo_angular_link_http__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_shoose_profile_shoose_profile__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_login_login__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common_http__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* Cibercolegios */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_shoose_profile_shoose_profile__["a" /* ShooseProfilePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_7_apollo_angular_link_http__["b" /* HttpLinkModule */],
                __WEBPACK_IMPORTED_MODULE_6_apollo_angular__["b" /* ApolloModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* Cibercolegios */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* Cibercolegios */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_shoose_profile_shoose_profile__["a" /* ShooseProfilePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */],
                    useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                Storage,
                __WEBPACK_IMPORTED_MODULE_4__utils_helper__["a" /* Helper */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cibercolegios; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_profile_profile__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_shoose_profile_shoose_profile__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__utils_helper__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var Cibercolegios = (function () {
    function Cibercolegios(platform, statusBar, splashScreen, helper) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.helper = helper;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        this.initializePages();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'Profile', component: __WEBPACK_IMPORTED_MODULE_5__pages_profile_profile__["a" /* ProfilePage */] },
            { title: 'ShooseProfile', component: __WEBPACK_IMPORTED_MODULE_6__pages_shoose_profile_shoose_profile__["a" /* ShooseProfilePage */] },
            { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */] }
        ];
    }
    Cibercolegios.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    Cibercolegios.prototype.initializePages = function () {
        this.pages = [];
    };
    Cibercolegios.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], Cibercolegios.prototype, "nav", void 0);
    Cibercolegios = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Applications/Xampp/xamppfiles/htdocs/cibercolegios/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__utils_helper__["a" /* Helper */]])
    ], Cibercolegios);
    return Cibercolegios;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Helper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(110);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { Links } from './links';




var Helper = (function () {
    function Helper(storage, alert, http, toast, loading, modal) {
        this.storage = storage;
        this.alert = alert;
        this.http = http;
        this.toast = toast;
        this.loading = loading;
        this.modal = modal;
        this.TOKEN = 'Token';
        this.ID = 'id';
        this.NAME = 'name';
        this.NONE = 'none';
        this.EMAIL = 'email';
        this.PROFILES = 'profiles';
        this.profiles = {};
        this.loader = this.loading.create({
            spinner: 'hide',
            dismissOnPageChange: true
        });
        this.data = {};
    }
    /**
     * Keep user on local storage
     */
    Helper.prototype.saveUser = function (token, id, name, email, profiles) {
        var _this = this;
        if (profiles === void 0) { profiles = {}; }
        return new Promise(function (resolve, reject) {
            _this.storage.set(_this.TOKEN, token).then(function (val) {
                _this.token = token;
                _this.storage.set(_this.ID, id).then(function (val) {
                    _this.id = id;
                    _this.storage.set(_this.NAME, name).then(function (val) {
                        _this.name = name;
                        _this.storage.set(_this.EMAIL, email).then(function (val) {
                            _this.email = email;
                            _this.storage.set(_this.PROFILES, profiles).then(function (val) {
                                _this.profiles = profiles;
                                resolve(val);
                            });
                        }, function (error) { return reject(error); }); //email
                    }, function (error) { return reject(error); }); //name
                }, function (error) { return reject(error); }); //id
            }, function (error) { return reject(error); }); //Token
        });
    };
    Helper.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.remove(_this.TOKEN)
                .then(function (val) {
                _this.token = undefined;
                _this.storage.remove(_this.id)
                    .then(function (val) {
                    _this.id = undefined;
                    _this.storage.remove(_this.NAME)
                        .then(function (val) {
                        _this.name = undefined;
                        _this.storage.remove(_this.EMAIL)
                            .then(function (val) {
                            _this.email = undefined;
                            resolve(val);
                        });
                    }, function (error) { return reject(error); }); //name
                }, function (error) { return reject(error); }); //id
            }, function (error) { return reject(error); }); //token
        });
    };
    Helper.prototype.isLogin = function () {
        return (this.token !== undefined &&
            this.token !== null &&
            this.id !== undefined &&
            this.id !== null &&
            this.email !== undefined &&
            this.email !== null);
    };
    Helper.prototype.isLogout = function () {
        return this.token === undefined &&
            this.id === undefined &&
            this.email === undefined;
    };
    Helper.prototype.showConnError = function (error) {
        if (error === void 0) { error = 'no error'; }
        this.alert.create({
            title: 'Error de red',
            subTitle: 'Error al conectarse al servidor, por favor intentalo de nuevo más tarde',
            buttons: ['Ok']
        }).present();
    };
    Helper.prototype.toastMessage = function (message, duration) {
        if (duration === void 0) { duration = 3000; }
        this.toast.create({
            message: message,
            duration: duration
        }).present();
    };
    Helper.prototype.incorrectSession = function () {
        this.toastMessage('Iniciaste sessión en otro dispositivo, por favor inicia de nuevo');
        this.logout().then(function (val) {
            // this.app.initializePages();
        }, function (error) {
            // this.app.initializePages();
        });
    };
    Helper = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* LoadingController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* ModalController */]) === "function" && _f || Object])
    ], Helper);
    return Helper;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=helper.js.map

/***/ })

},[245]);
//# sourceMappingURL=main.js.map