import { Component } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Helper } from './../../utils/helper';
import { NavController, NavParams, Loading } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

import { ShooseProfilePage } from '../shoose-profile/shoose-profile';
import { ProfilePage } from './../profile/profile';

import { ApolloClient, ApolloQueryResult } from 'apollo-client';
import { HttpLink } from 'apollo-angular-link-http';
import { HttpHeaders } from '@angular/common/http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, concat } from 'apollo-link';
import { graphQLResultHasError } from 'apollo-utilities';


const Login = gql`
  query Login ($username:String!, $password:String!, $mobileId:String!){
    login(username: $username, password: $password, mobileId: $mobileId) {
      token
      id
      name
      lastName
      passwordChange
      email
      profiles{
        profileIdCon
        profileId
        institutionId
        institutionDesc
        profileDesc
      }
    }
  }`;


  @Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})


export class LoginPage  {

  private loginForm;
  private model: any = {};

  public protocol: string;
  public host: string;
  public home: string;

  private mobile = "MOB-88768";



  constructor(
    private formBuilder: FormBuilder,
    private navCtrl: NavController,
    private navParams: NavParams,
    private http: Http,
    private helper: Helper,
    private apollo: Apollo,
    private httpLink: HttpLink,
    private platform: Platform
    
  ) {
    if (this.platform.is('core')) {
      this.protocol = "http";
      this.host = "localhost:8100/cibercolegios"
  } else {
      this.protocol = "https";
      this.host = "appstest.cibercolegios.com";

  }
    this.home = this.protocol + "://" + this.host;
  
    this.ionViewDidLoad();

    const uri = this.home + '/api/security/';
    const method = 'POST';
    // const headers = new HttpHeaders ({
    //   'Content-Type': 'application/json'
    // });
    

    
    const authMiddleware = new ApolloLink((operation, forward) => {
      
      operation.setContext({
        headers: {
          'Content-Type': 'text/plain'
        } 
      });
      
      return forward(operation);
    })
    
    const queryLink = httpLink.create({ uri, method });
    
    apollo.create({
      link: concat(authMiddleware, queryLink),
      cache: new InMemoryCache(),
    })

    // this.user = navParams.data || new User();
  
  }

  ionViewDidLoad() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  
  sendLogin(){

    this.helper.toastMessage('Iniciando Session');

    
    let body = {
        username: this.loginForm.value.username,
        password: this.loginForm.value.password,
        mobileId: this.mobile
    };
    let params = Object.assign(body);
    console.log('los campos son ', params);
    
    let loginQuery = this.apollo.query({query: Login, variables: params})
    .subscribe(
      
        data => this.loginOk(data),
        errors => this.loginError(errors),
        
      );
  }

  loginOk (queryResult: ApolloQueryResult<{}>){
    
    let query = queryResult.data;
    console.log('query final', query);
    this.navCtrl.setRoot(ProfilePage);
    
    // if (
    //   query.token && 
    //   query.id &&
    //   query.name &&
    //   query.email &&
    //   query.profiles
    // ) {
    //   this.helper.saveUser(
    //     query.token,
    //     query.id,
    //     query.name,
    //     query.email,
    //     query.profiles
    //   ).then(val => {
        
    //     if (this.helper.NONE === this.helper.id) {
    //       this.navCtrl.setRoot(ProfilePage);
    //     } else {
    //       this.navCtrl.setRoot(ShooseProfilePage);
    //     }
    // },
    // error => {
    //     console.log('Error on login Ok save the user');
    //     console.log(error);
    // });
    // }
  }

  loginError (queryError: ApolloQueryResult<{}>) {
    let errors = queryError.errors;
    console.log('error del login',errors);
    this.navCtrl.setRoot(LoginPage);
  }


  saveUser(user: Object){

  }

}