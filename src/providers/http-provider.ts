import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HttpProvider {

  constructor(
    private http: Http,
    private headers: Headers

    ) {
    console.log('Hello HttpProvider Provider');
  }

getRemoteData (){
  console.log(this.http.get('../assets/bar.json'));
}
// preflight () {
//   this.headers  = new Headers ({
//       'Method': 'POST',
//   });
// }
}