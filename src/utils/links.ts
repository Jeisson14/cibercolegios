// import { Injectable } from '@angular/core';
// import { Platform } from 'ionic-angular';

// @Injectable()
// export class Links {

//     public protocol: string;

//     public coach: string;

//     public host: string;

//     public login: string;

//     public home: string;

//     public register: string;

//     public members: string;

//     public member: string;

//     public player: string;

//     public playerUrl: string;

//     public team: string;

//     public referee: string;

//     public refereeUrl: string;

//     public service: string;

//     public settings: string;

//     public uploads: string;

//     public palmaresAdd: string;

//     public scene: string;

//     public sceneUrl: string;

//     public serviceCreate: string;

//     public serviceEdit: string;

//     public servicePost: string;

//     public playerPost: string;

//     public refereePost: string;

//     public profileEvent: string;

//     public serviceTime: string;

//     public scenePost: string;

//     public threadAdd: string;

//     public viewThread: string;

//     public commentAdd: string;

//     public viewComments: string;

//     public viewComments2: string;

//     public events: string;

//     public addServiceTime: string;

//     public editServiceTime: string;

//     public deleteServiceTime: string;

//     public reset: string;

//     public search: string;

//     public memberSearch: string;

//     public chat: string;

//     public event: string;

//     public invitationEvent: string;

//     public eventCreate: string;

//     public eventEdit: string;

//     public eventView: string;

//     public deleteEvent: string;

//     public addEvent: string;

//     public messageThread: string;

//     public messageAdd: string;

//     public chatView: string;

//     public chatAdd: string;

//     public readChat: string;

//     public getChat: string;

//     public updateMessage: string;

//     public isRead: string;

//     public image: string;

//     public teamCreate: string;

//     public viewTeam: string;

//     public viewTeamUser: string;

//     public teamEdit: string;

//     public profileTeam: string;

//     public userMember: string;

//     public isAdministrator: string;

//     public usersTeam: string;

//     public addUsers: string;

//     public searchUser: string;

//     public teamUser: string;

//     public teamUserNew: string;

//     public fabebook: string;

//     public google: string;

//     public twitter: string;

//     public user: string;

//     public saveUser: string;

//     public countries: string;

//     public markRead: string;

//     public reservations: string;

//     public serviceUrl: string;

//     public serviceImages: string;

//     public servicePlaces: string;

//     public threadEvent: string;

//     public invitationEventPost: string;

//     public invitationEventUser: string;

//     public invitationEventEvent: string;

//     public serviceImage: string;

//     public eventsByUser: string;

//     public invitationAccept: string;

//     public invitationDecline: string;

//     public invitationCancel: string;

//     public eventCancel: string;

//     public teamsByUser: string;

//     public teamImage: string;

//     public userTeamsByUser: string;

//     public userTeamsByTeam: string;

//     public saveUserTeam: string;

//     public playerImage: string;

//     public formationByTeam: string;

//     public getTemplates: string;

//     public saveTemplate: string;

//     public saveLine: string;

//     public linesByTemplate: string;

//     constructor(private platform: Platform) {
//         if (this.platform.is('core')) {
//             this.protocol = "http";
//             this.host = "localhost:8100/tupalmares"
//         } else {
//             this.protocol = "https";
//             this.host = "tupalmares.com";

//         }
//         this.home = this.protocol + "://" + this.host;

//         this.coach = this.home + "/members/player/";

//         this.event = this.home + "/members/event/";

//         this.login = this.home + "/mobile/login.json";

//         this.register = this.home + "/mobile/register.json";

//         this.members = this.home + "/members/";

//         this.player = this.home + "/members/player/";

//         this.referee = this.home + "/members/referee/";

//         this.reset = this.home + "/resetting/request";

//         this.scene = this.home + "/members/scene/";

//         this.team = this.home + "/members/team/";

//         this.service = this.home + "/members/service/";

//         this.settings = this.home + "/settings/";

//         this.serviceCreate = this.home + "/profile/service/create.json";

//         this.serviceEdit = this.home + "/profile/service/{service}/post.json";

//         this.uploads = this.home + '/uploads/documents/';

//         this.palmaresAdd = this.home + "/profile/palmares/post.json";

//         this.playerPost = this.home + "/profile/footballplayer/post.json";

//         this.scenePost = this.home + "/profile/scene/post.json";

//         this.serviceTime = this.home + "/members/serviceTime/{id}.json";

//         this.refereePost = this.home + "/profile/footballreferee/post.json";

//         this.image = this.home + "/profile/footballreferee/image.json";

//         this.threadAdd = this.home + "/publication.json";

//         this.viewThread = this.home + "/publication/thread/{user}.json";

//         this.addServiceTime = this.home + "/availability/postnewservice/{id}.json";

//         this.editServiceTime = this.home + "/availability/postservice/{availability}/{serviceTime}.json";

//         this.events = this.home + "/availability/{id}/events.json";

//         this.deleteServiceTime = this.home + "/availability/serviceTime/r/{serviceTime}.json";

//         this.commentAdd = this.home + "/publication/{thread}/comments.json";

//         this.viewComments = this.home + "/publication/{thread}/comments.json";

//         this.viewComments2 = this.home + "/comment/{thread}.json";

//         this.search = this.home + '/mobile/search.json';

//         this.chat = this.home + '/chat/chats/{user}.json';

//         this.messageThread = this.home + '/chat/chat/{messageThread}.json';

//         this.messageAdd = this.home + '/chat/chat/{messageThread}.json';

//         this.chatView = this.home + '/chat/messages/{chat}.json';

//         this.chatAdd = this.home + '/chat/message/{receiver}.json';

//         this.readChat = this.home + '/chat/unread/user.json';

//         this.updateMessage = this.home + '/chat/messages/update/{thread}.json';

//         this.isRead = this.home + '/chat/isread/{thread}.json';

//         this.teamCreate = this.home + '/profile/team/post.json';

//         this.viewTeam = this.home + '/administrador/teams/view/{footballPlayer}.json';

//         this.teamEdit = this.home + '/administrador/team/{team}/post.json';

//         this.profileTeam = this.home + '/team/{team}.json';

//         this.userMember = this.home + '/administrador/teams/member/{user}.json';

//         this.isAdministrator = this.home + '/administrador/teams/administrator/{team}.json';

//         this.usersTeam = this.home + '/administrador/users/team/{team}.json';

//         this.addUsers = this.home + '/administrador/add/participant/{user}';

//         this.searchUser = this.home + '/administrador/search/user/{userName}.json';

//         this.teamUserNew = this.home + '/profile/team/{user}/{team}.json';

//         this.teamUser = this.home + '/teams/user/';

//         this.playerUrl = this.home + '/fileupload/player/{footBallPlayer}';

//         this.refereeUrl = this.home + '/fileupload/referee/{footBallReferee}';

//         this.sceneUrl = this.home + '/fileupload/scene/{scene}';

//         this.google = this.home + '/mobile/google.json';

//         this.google = this.home + '/mobile/twitter.json';

//         this.user = this.home + '/members/user/{user}.json';

//         this.countries = this.home + '/members/countries/all';

//         this.saveUser = this.home + '/users/edit';

//         this.fabebook = this.home + '/members/username/user/{username}.json';

//         this.invitationEvent = this.home + '/users/viewevent/';

//         this.eventView = this.home + '/users/viewevent/{user}.json';

//         this.addEvent = this.home + '/profile/event/post/{service}.json';

//         this.eventEdit = this.home + '/profile/event/post/{event}/post.json';

//         this.deleteEvent = this.home + "/profile/event/r/{event}.json";

//         this.serviceUrl = this.home + '/uploads/service/mobile/{service}.json';

//         this.serviceImages = this.home + '/uploads/service/{service}.json';

//         this.member = this.home + '/members/member/mobile/{user}.json';

//         this.markRead = this.home + '/chat/message/read/all/{chat}.json';

//         this.getChat = this.home + '/chat/chat/{receiver}.json';

//         this.reservations = this.home + '/profile/event/all/{service}.json';

//         this.servicePlaces = this.home + '/profile/service/{service}/places.json';

//         this.threadEvent = this.home + '/profile/event/thread/get/{event}.json';

//         this.invitationEventPost = this.home + '/profile/invitation/{user}/{event}.json';

//         this.invitationEventUser = this.home + '/profile/invitation/user/{user}/all.json';

//         this.invitationEventEvent = this.home + '/profile/invitation/event/{event}/all.json';

//         this.memberSearch = this.home + '/mobile/member/search.json';

//         this.serviceImage = this.home + '/uploads/service/{service}?random=1';

//         this.eventsByUser = this.home + '/profile/event/user/{user}/slice.json';

//         this.invitationAccept = this.home + '/profile/event/invitation/{invitation}/accept.json';

//         this.invitationDecline = this.home + '/profile/event/invitation/{invitation}/decline.json';

//         this.invitationCancel = this.home + '/profile/event/invitation/{invitation}/cancel.json';

//         this.eventCancel = this.home + '/profile/event/{event}/cancel.json';

//         this.teamsByUser = this.home + '/profile/team/user/{user}/all.json';

//         this.teamImage = this.home + '/bundles/web/images/team.png';

//         this.userTeamsByUser = this.home + '/profile/team/userteams/{user}/all.json';

//         this.userTeamsByTeam = this.home + '/profile/team/userteams/{team}/team/slice.json';

//         this.saveUserTeam = this.home + '/profile/team/{user}/{team}.json';

//         this.playerImage = this.home + '/bundles/web/images/soccer.png';

//         this.formationByTeam = this.home + '/profile/template/formation/team/{team}.json';

//         this.getTemplates = this.home + '/profile/template/gametemplates/all.json';

//         this.saveTemplate = this.home + '/profile/template/create/template.json';

//         this.saveLine = this.home + '/profile/template/create/line/{template}.json';

//         this.linesByTemplate = this.home + '/profile/template/gametemplate/lines/{template}.json';
//     }

//     /**
//      * Configure link to use in local tests
//      * 1. replace the local prefix in link
//      */
//     public replaceLocalPrefix(link: string): string {
//         // 1.
//         if (link)
//             link = this.home + link.replace('palmares/web/', '');

//         return link;
//     }
// }