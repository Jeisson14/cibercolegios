import { Cibercolegios } from './../app/app.component';
// import { Links } from './links';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, ToastController, LoadingController, Loading, ModalController } from 'ionic-angular';
import { Http, Headers, Response } from '@angular/http';

export type optionType = { name: string, value: number };

@Injectable()
export class Helper {


    public readonly TOKEN = 'Token';

    public readonly ID = 'id';

    public readonly NAME = 'name';

    public readonly NONE = 'none';

    public readonly EMAIL = 'email';

    public readonly PROFILES = 'profiles';

    public app: Cibercolegios;

    public token: string;

    public id: string;

    public name: string;

    public loader: Loading;

    public email: string;

    public profiles: any = {}; 

    public data: any;

    constructor(
        private storage: Storage,
        private alert: AlertController,
        private http: Http,
        private toast: ToastController,
        private loading: LoadingController,
        private modal: ModalController,
        // private links: Links
    ) {
        this.loader = this.loading.create({
            spinner: 'hide',
            dismissOnPageChange: true
        });
        this.data = {};
    }    

    /**
     * Keep user on local storage
     */
    saveUser(token: string, id: string, name: string, email: string, profiles: any = {}) {
        return new Promise((resolve, reject) => {
            this.storage.set(this.TOKEN, token).then(val => {
                this.token = token;
                this.storage.set(this.ID, id).then(val => {
                    this.id = id;
                    this.storage.set(this.NAME, name).then(val => {
                        this.name = name;
                        this.storage.set(this.EMAIL, email).then(val => {
                            this.email = email;
                            this.storage.set(this.PROFILES, profiles).then(val => {
                                this.profiles = profiles;
                                resolve(val);
                            })
                        },
                            error => reject(error));//email
                    },
                        error => reject(error));//name
                },
                    error => reject(error));//id
            },
                error => reject(error));//Token
        });
    }

    logout() {
        return new Promise((resolve, reject) => {
            this.storage.remove(this.TOKEN)
                .then(val => {
                    this.token = undefined;
                    this.storage.remove(this.id)
                        .then(val => {
                            this.id = undefined;
                            this.storage.remove(this.NAME)
                                .then(val => {
                                    this.name = undefined;
                                    this.storage.remove(this.EMAIL)
                                        .then(val => {
                                            this.email = undefined;
                                            resolve(val);
                                        })
                                },
                                error => reject(error));//name
                        },
                        error => reject(error));//id
                        
                },
                error => reject(error));//token
        });
    }

    isLogin(): boolean {
        return (this.token !== undefined &&
            this.token !== null &&
            this.id !== undefined &&
            this.id !== null &&
            this.email !== undefined &&
            this.email !== null
        )
            ;

    }

    isLogout(): boolean {
        return this.token === undefined &&
            this.id === undefined &&
            this.email === undefined;
    }

    showConnError(error: any = 'no error') {
        this.alert.create({
            title: 'Error de red',
            subTitle: 'Error al conectarse al servidor, por favor intentalo de nuevo más tarde',
            buttons: ['Ok']
        }).present();
    }


    toastMessage(message: string, duration: number = 3000) {
        this.toast.create({
            message: message,
            duration: duration
        }).present();
    }

    incorrectSession() {
        this.toastMessage('Iniciaste sessión en otro dispositivo, por favor inicia de nuevo');
        this.logout().then(val => {
            // this.app.initializePages();
        }, error => {
            // this.app.initializePages();
        });
    }

}