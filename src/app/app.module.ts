import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { Helper } from './../utils/helper';
import { IonicStorageModule } from '@ionic/storage';
import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
// import { HttpLink } from 'apollo-link-http';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';



import { Cibercolegios } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { ShooseProfilePage } from '../pages/shoose-profile/shoose-profile';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule, HttpHeaders,  } from '@angular/common/http';


@NgModule({
  declarations: [
    Cibercolegios,
    HomePage,
    ProfilePage,
    ShooseProfilePage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    HttpLinkModule,
    ApolloModule,
    IonicModule.forRoot(Cibercolegios),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Cibercolegios,
    HomePage,
    ProfilePage,
    ShooseProfilePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, 
      useClass: IonicErrorHandler},
    Storage,
    Helper
  ]
})

export class AppModule {}

